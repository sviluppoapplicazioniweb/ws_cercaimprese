<?php

namespace App\Classes;

//use Illuminate\Database\Eloquent\Model;

//class Localizzazione extends Model {
class Localizzazione {

protected $insegna = "";
protected $nREALoc;
protected $stato = "";
protected $provincia = "";
protected $comune = "";
protected $frazione = "";
protected $indirizzo = "";
protected $cap = "";
protected $attività;// = "";
protected $ateco = "";
protected $comuneISTAT;// = "";
protected $tipologie = "";
protected $tipoLocalizzazione = "";
protected $tipoSede = "";
protected $telefono = "";
protected $fax = "";
protected $pec = "";

    /**
     * Costruttore
     */
    public function _construct() {
        // TODO Auto-generated constructor stub
    }

    /**
     * @return the nREALoc
     */
    public function getNREALoc() {
        return $this->nREALoc;
    }

    /**
     * @param numeroREA the nREALoc to set
     */
    public function setNREALoc($numeroREA) {
        $this->nREALoc = $numeroREA;
    }

	/**
     * @return the pec
     */
	public function getPec() {
		return $this->pec;
	}

	/**
     * @param pec to set
     */
	public function setPec($pec) {
        $this->pec = $pec;
    }

	/**
     * @return the telefono
     */
	public function getTelefono() {
		return $this->telefono;
	}

	/**
     * @param telefono to set
     */
	public function setTelefono($telefono) {
        $this->telefono = $telefono;
}

	/**
     * @return the fax
     */
	public function getFax() {
		return $this->fax;
	}

	/**
     * @param fax to set
     */
	public function setFax($fax) {
        $this->fax = $fax;
    }

	/**
     * @return the tipologie di localizzazione
     */
	public function getTipoLocalizzazione() {
		return $this->tipoLocalizzazione;
	}

	/**
     * @param tipologie the Localizzazione to set
     */
	public function setTipoLocalizzazione($tipoLocalizzazione) {
        $this->tipoLocalizzazione = $tipoLocalizzazione;
    }

	/**
     * @return the tipologie
     */
	public function getTipologie() {
		return $this->tipologie;
	}

	/**
     * @param tipologie the tipologie to set
     */
	public function setTipologie($tipologie) {
        $this->tipologie = $tipologie;
    }

	/**
     * @return the tipoSede
     */
	public function getTipoSede() {
		return $this->tipoSede;
	}

	/**
     * @param tipoSede the tipoSede to set
     */
	public function setTipoSede($tipoSede) {
        $this->tipoSede = $tipoSede;
    }

	/**
     * @return the attività
     */
	public function getAttività() {
		return $this->attività;
	}


	/**
     * @param attività the attività to set
     */
	public function setAttività($attività) {
        $this->attività = $attività;
    }

	/**
     * @return the ateco
     */
	public function getAteco() {
		return $this->ateco;
	}

	/**
     * @param ateco the ateco to set
     */
	public function setAteco($ateco) {
        $this->ateco = $ateco;
    }
	/**
     * @return the comuneISTAT
     */
	public function getComuneISTAT() {
		return $this->comuneISTAT;
	}

	/**
     * @param comuneISTAT the comuneISTAT to set
     */
	public function setComuneISTAT($comuneISTAT) {
        $this->comuneISTAT = $comuneISTAT;
    }

	/**
     * @return the insegna
     */
	public function getInsegna() {
		return $this->insegna;
	}

	/**
     * @param insegna the insegna to set
     */
	public function setInsegna($insegna) {
        $this->insegna = $insegna;
    }

	/**
     * @return the stato
     */
	public function getStato() {
		return $this->stato;
	}

	/**
     * @param stato the stato to set
     */
	public function setStato($stato) {
        $this->stato = $stato;
    }

	/**
     * @return the provincia
     */
	public function getProvincia() {
		return $this->provincia;
	}

	/**
     * @param provincia the provincia to set
     */
	public function setProvincia($provincia) {
        $this->provincia = $provincia;
    }

	/**
     * @return the comune
     */
	public function getComune() {
		return $this->comune;
	}

	/**
     * @param comune the comune to set
     */
	public function setComune($comune) {
        $this->comune = $comune;
    }

	/**
     * @return the frazione
     */
	public function getFrazione() {
		return $this->frazione;
	}

	/**
     * @param frazione the frazione to set
     */
	public function setFrazione($frazione) {
        $this->frazione = $frazione;
    }

	/**
     * @return the indirizzo
     */
	public function getIndirizzo() {
		return $this->indirizzo;
	}

	/**
     * @param indirizzo the indirizzo to set
     */
	public function setIndirizzo($indirizzo) {
        $this->indirizzo = $indirizzo;
    }

	/**
     * @return the cap
     */
	public function getCap() {
		return $this->cap;
	}

	/**
     * @param cap the cap to set
     */
	public function setCap($cap) {
        $this->cap = $cap;
    }

}
