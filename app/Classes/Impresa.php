<?php namespace App\Classes;

use App\Localizzazione;

class Impresa {

	protected $denominazione = '';
	protected $numeroREA = 0;
	protected $codiceFiscale = "";
	protected $partitaIVA = "";
	protected $indirizzoSede = "";
	protected $comuneSede = "";
	protected $CAP = "";
	protected $Frazione = "";
	protected $TelSede = "";
	protected $FaxSede = "";
	protected $PECSede = "";
	protected $TipoSede = "";
	protected $attivita;
	protected $limit;

	protected $localizzazioni = null;
	protected $oggettoSociale = "";
	protected $provincia = "";

	/**
	 * Costruttore
	 */
	public function _construct($denominazione, $numeroREA, $codiceFiscale, $partitaIVA , $indirizzoSede, $oggettoSociale,
			$tiposede, $varattivita, $limit, $localizzazioni) {

		$this->denominazione = $denominazione;
		$this->numeroREA = $numeroREA;
		$this->codiceFiscale = $codiceFiscale;
		$this->partitaIVA = $partitaIVA;
		$this->indirizzoSede = $indirizzoSede;
		$this->oggettoSociale = $oggettoSociale;
		$this->TipoSede = $tiposede;
		$this->attivita = $varattivita;
		$this->limit = $limit;
		$this->localizzazioni = $localizzazioni;
	}

	/**
	 * @return the PEC Sede
	 */
	public function getPECSede() {
		return $this->PECSede;
	}

	/**
	 * @param PECSede per settare la PEC della Sede
	 */
	public function setPECSede($PECSede) {
		$this->PECSede = $PECSede;
	}

		/**
		 * @return the Fax Sede
		 */
	public function getFaxSede() {
		return $this->FaxSede;
	}

		/**
		 * @param FaxSede per settare il Fax della Sede
		 */
	public function setFaxSede($FaxSede) {
		$this->FaxSede = $FaxSede;
	}

		/**
		 * @return the Telefono Sede
		 */
	public function getTelSede() {
		return $this->TelSede;
	}

	/**
	 * @param TelSede per settare il Telefono della Sede
	 */
	public function setTelSede($TelSede) {
		$this->TelSede = $TelSede;
	}

	/**
	 * @return the Frazione Sede
	 */
	public function getFrazione() {
		return $this->Frazione;
	}

	/**
	 * @param Frazione per settare la Frazione della Sede
	 */
	public function setFrazione($Frazione) {
		$this->Frazione = $Frazione;
	}

	/**
	 * @return the CAP Sede
	 */
	public function getCAP() {
		return $this->CAP;
	}

	/**
	 * @param CAP per settare il CAP della Sede
	 */
	public function setCAP($CAP) {
		$this->CAP = $CAP;
	}

	/**
	 * @return the comune Sede
	 */
	public function getComuneSede() {
		return $this->comuneSede;
	}

	/**
	 * @param comuneSede per settare il comune della Sede
	 */
	public function setComuneSede($comuneSede) {
		$this->comuneSede = $comuneSede;
	}

	/**
	 * @return the provincia
	 */
	public function getProvincia() {
		return $this->provincia;
	}

	/**
	 * @param provincia the provincia to set
	 */
	public function setProvincia($provincia) {
		$this->provincia = $provincia;
	}

	/**
	 * @return the denominazione
	 */
	public function getDenominazione() {
		return $this->denominazione;
	}

	/**
	 * @param denominazione the denominazione to set
	 */
	public function setDenominazione($denominazione) {
		$this->denominazione = $denominazione;
	}

	/**
	 * @return the numeroREA
	 */
	public function getNumeroREA() {
		return $this->numeroREA;
	}

	/**
	 * @param numeroREA the numeroREA to set
	 */
	public function setNumeroREA($numeroREA) {
		$this->numeroREA = $numeroREA;
	}

	/**
	 * @return the codiceFiscale
	 */
	public function getCodiceFiscale() {
		return $this->codiceFiscale;
	}

	/**
	 * @param codiceFiscale the codiceFiscale to set
	 */
	public function setCodiceFiscale($codiceFiscale) {
		$this->codiceFiscale = $codiceFiscale;
	}

	/**
	 * @param partitaIVA the partitaIVA to set
	 */
	public function setPartitaIVA($partitaIVA) {
		$this->partitaIVA = $partitaIVA;
	}

	/**
	 * @return the partitaIVA
	 */
	public function getPartitaIVA() {
		return $this->partitaIVA;
	}

	/**
	 * @return the indirizzoSede
	 */
	public function getIndirizzoSede() {
		return $this->indirizzoSede;
	}

	/**
	 * @param indirizzoSede the indirizzoSede to set
	 */
	public function setIndirizzoSede($indirizzoSede) {
		$this->indirizzoSede = $indirizzoSede;
	}

	/**
	 * @return the localizzazioni
	 */
	public function getLocalizzazioni() {
		return $this->localizzazioni;
	}

	/**
	 * @param localizzazioni the localizzazioni to set
	 */
	public function setLocalizzazioni($localizzazioni) {
		$this->localizzazioni = $localizzazioni;
	}

	/**
	 * @return the oggettoSociale
	 */
	public function getOggettoSociale() {
		return $this->oggettoSociale;
	}

	/**
	 * @param oggettoSociale the oggettoSociale to set
	 */
	public function setOggettoSociale($oggettoSociale) {
		$this->oggettoSociale = $oggettoSociale;
	}


	/**
	 * @return il tipo sede, troncato alla lunghezza maxLength
	 */
	public function getTipoLocalizzazione() {
		// TODO Auto-generated method stub
		return $this->TipoSede;
	}

	/**
	 * @param tipoSede the tipoSede to set
	 */
	public function setTipoLocalizzazione($tipoSede) {
		// TODO Auto-generated method stub
		$this->TipoSede = $tipoSede;
	}

	/**
	 * @return il attività, troncato alla lunghezza maxLength
	 */
	public function getAttività() {
		// TODO Auto-generated method stub
		return $this->attivita;
	}

	/**
	 * @param varattivita the attivita to set
	 */
	public function setAttività($varattivita) {
		// TODO Auto-generated method stub
		$this->attivita = $varattivita;
	}

	/**
	 * @return il limite, troncato alla lunghezza maxLength
	 */
	public function getLimit() {
		// TODO Auto-generated method stub
		return $this->limit;
	}

	/**
	 * @param limit the limit to set
	 */
	public function setLimit($limit) {
		// TODO Auto-generated method stub
		$this->limit = $limit;
	}

	public function toArray(){
		return (array) $this;
	}
}
