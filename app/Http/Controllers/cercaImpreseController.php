<?php namespace App\Http\Controllers;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Classes\Impresa;
use App\Classes\Localizzazione;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use SoapBox\Formatter\Formatter;
use Illuminate\Support\Facades\Log;

class cercaImpreseController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//$results = DB::select('select * from imprese_tot where cciaa = \'MI\' AND n_rea=\'2031933\'')->get();


		//print_r($results);
		//return Response::json($results);

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * @param $c_t_ul_1
	 * @param $c_t_ul_2
	 * @param $c_t_ul_3
	 * @param $c_t_ul_4
	 * @param $c_t_ul_5
	 * @return string
     */
	protected function decodeTipiLocalizzazione($c_t_ul_1, $c_t_ul_2, $c_t_ul_3, $c_t_ul_4, $c_t_ul_5)
	{

		$c_t_ul_tot = '';
		if (strlen($c_t_ul_1)>0)
			$c_t_ul_tot = $c_t_ul_1;
		if (strlen($c_t_ul_2)>0)
			$c_t_ul_tot .= $c_t_ul_2;
		if (strlen($c_t_ul_3)>0)
			$c_t_ul_tot .= $c_t_ul_3;
		if (strlen($c_t_ul_4)>0)
			$c_t_ul_tot .= $c_t_ul_4;
		if (strlen($c_t_ul_5)>0)
			$c_t_ul_tot .= $c_t_ul_5;

		return $c_t_ul_tot;
	}


	protected function WriteXML($imprese){
		$writer = new \Sabre\Xml\Writer();
		$writer->openMemory();
		/*$writer->namespaceMap = [
			'http://example.org/' => 'ListaImprese',
		];*/
		$writer->startDocument();
		$writer->startElement('listaImprese');

		//Scorre la collection e costruisce l'XML
		foreach ($imprese as $impresa){
			$this->writeNodeImpresa($impresa, $writer);
		}

		$writer->endElement();
		return $writer->outputMemory();
	}

	protected function writeNodeImpresa($impresa, $xml){

		$xml->startElement('imprese');
		$xml->writeElement('NumeroREA',$impresa->getNumeroREA());
		$xml->writeElement('codiceFiscale',$impresa->getCodiceFiscale());
		$xml->writeElement('PartitaIVA', $impresa->getPartitaIVA());
		$xml->writeElement('IndirizzoSede', $impresa->getIndirizzoSede());
		$xml->writeElement('CAP', $impresa->getCAP());
		$xml->writeElement('ComuneSede', $impresa->getComuneSede());
		$xml->writeElement('Frazione' , $impresa->getFrazione());
		$xml->writeElement('TelSede', $impresa->getTelSede());
		$xml->writeElement('FaxSede', $impresa->getFaxSede());
		$xml->writeElement('PECSede' , $impresa->getPECSede());

		if (!is_null($impresa->getLocalizzazioni())) {
			$localizzazioni = $impresa->getLocalizzazioni();

			foreach ($localizzazioni as $localizzazione) {
				$xml->startElement('localizzazione');
				$xml->writeElement('ateco' , $localizzazione->getAteco());
				$xml->writeElement('cap' , $localizzazione->getCap());
				$xml->writeElement('comune', $localizzazione->getComune());
				$xml->writeElement('frazione' , $localizzazione->getFrazione());
				$xml->writeElement('indirizzo' , $localizzazione->getIndirizzo());
				$xml->writeElement('insegna' , $localizzazione->getInsegna());
				$xml->writeElement('NREALoc' , $localizzazione->getNREALoc());
				$xml->writeElement('pec', $localizzazione->getPec());
				$xml->writeElement('provincia', $localizzazione->getProvincia());
				$xml->writeElement('stato' , $localizzazione->getStato());
				$xml->writeElement('tipoLocalizzazione', $localizzazione->getTipoLocalizzazione());
				$xml->writeElement('tipoSede' , $localizzazione->getTipoSede());
				$xml->writeElement('tipologie' , $localizzazione->getTipologie());
				//End element localizzazione
				$xml->endElement();
			}


		} else {
			$xml->writeElement('localizzazione' , '');
		}

		//End element imprese
		$xml->endElement();

	}


	/**
	 * @param Request $request
	 * @param null $cf
	 * @param null $nrea
	 * @param null $denominazione
	 * @param null $attivita
	 * @param null $prv
	 * @param null $comune
	 * @param null $json
     * @return mixed
     */
	public function find(Request $request)
	{
		$connection = 'mysql_2';

		$cf = $request->input('cf');
		$nrea = $request->input('nrea');
		$denominazione = $request->input('denominazione');
		$attivita = $request->input('attivita');
		$prv = $request->input('prv');
		$comune = $request->input('comune');
		$json = $request->input('json');

		$credentials = $request->only('email', 'password');

		try {
			// attempt to verify the credentials and create a token for the user
			if (! $token = JWTAuth::attempt($credentials)) {
				Log::error('error => invalid_credentials: User '.$request->input('email').' - Password: '.$request->input('password'));
				return Response::make('error => invalid_credentials', 200)->header('Content-Type', 'application/json');
				//return response()->json(['error' => 'invalid_credentials'], 401);
			}
		} catch (JWTException $e) {
			// something went wrong whilst attempting to encode the token
			//return response()->json(['error' => 'could_not_create_token'], 500);
			Log::error('error => could_not_create_token.');
			return Response::make('error => could_not_create_token', 200)->header('Content-Type', 'application/json');
		}

		// all good so return the token
		//$token = response()->json(compact('token'));

		$rs_conn = DB::connection($connection);
		$rs_imprese = $rs_conn->table('imprese_tot');

		//$cf = '06561570968';
		//$nrea = 72;
		//$denominazione = 'AMSA';

		if ($rs_imprese && $nrea != null){
			$rs_imprese->where('n_rea',$nrea);
		}
		if ($rs_imprese && $cf != null){
			$rs_imprese->where('C_FISCALE_IMPRESA',$cf);
		}

		if ($rs_imprese && $denominazione != null){
			$rs_imprese->where('DENOMINAZIONE_SEDE','like','%'.strtoupper($denominazione).'%');
		}

		if ($rs_imprese && $attivita != null){
			$rs_imprese->where('desatt','like','%'.strtoupper($attivita).'%');
		}

		if ($rs_imprese && $prv != null){
			$rs_imprese->where('SGL_PRV',$prv);
		}

		if ($rs_imprese && $comune != null){
			$rs_imprese->where('C_COMUNE',$comune);
		}

		if ($rs_imprese){
			$rs_imprese
				->whereNull('DTCESAA_EFFETTIVA')
				->where('C_FONTE', '=', 'RI')
				->where('I_STATO_ATTIVITA','=','A')
				->orderBy('n_rea', 'asc')
				->orderBy('ul', 'asc');
		}

		//dd($rs_imprese->get());
		try {
			$ls_imprese = $rs_imprese->get();
		} catch (\mysqli_sql_exception $e){
			Log::error('error => invalid_sql_exception: '.$e);
		}

		//dd($ls_imprese);
		//return view('home');
		$imprese = collect();
		$ls_local = collect();

		$impTrovata = new \App\Classes\Impresa();

		$nrREA = 0;
		$attivita = null;
		$i=0;

		foreach ($ls_imprese as $rs_impresa)
		{
			/*echo "Denominazione: ".$rs_impresa->DENOMINAZIONE;
			return view('home');
			exit;*/
			if ($rs_impresa->ul == 0 or ($rs_impresa->ul > 0 && $nrREA != $rs_impresa->n_rea)) {
				$nrREA = $rs_impresa->n_rea;

				if ($impTrovata->getNumeroREA() > 0 && $nrREA != $impTrovata->getNumeroREA()) {

					if ($ls_local->count()>0) $impTrovata->setLocalizzazioni($ls_local);
					$imprese->push($impTrovata);
					//$this->writeNode($impTrovata, $writer);
					//$imprese = array_add($imprese, 'imprese', $impTrovata);

					//Reinizializza gli oggetti
					$impTrovata = new \App\Classes\Impresa();
					$ls_local = collect();
				}

				$impTrovata->setNumeroREA($rs_impresa->n_rea);
				$impTrovata->setCodiceFiscale($rs_impresa->C_FISCALE_IMPRESA);
				$impTrovata->setPartitaIVA($rs_impresa->PARTITA_IVA);
				$impTrovata->setIndirizzoSede($rs_impresa->C_VIA." ".$rs_impresa->VIA + "," + $rs_impresa->N_CIVICO);
				$impTrovata->setCAP($rs_impresa->CAP);
				$impTrovata->setComuneSede($rs_impresa->COMUNE);
				$impTrovata->setFrazione($rs_impresa->FRAZIONE);
				$impTrovata->setTelSede($rs_impresa->N_TELEFONO);
				$impTrovata->setFaxSede($rs_impresa->N_TELEFAX);
				$impTrovata->setPECSede($rs_impresa->INDIRIZZO_PEC);

				/*dd($impTrovata);
				exit;*/
			} else {
				//Unita locale
				$tipoSede = "";
				//Inserisce localizzazioni
				$localizzazione = new \App\Classes\Localizzazione();

				$localizzazione->setComune($rs_impresa->COMUNE);
				$localizzazione->setIndirizzo($rs_impresa->C_VIA + " " + $rs_impresa->VIA + ", " + $rs_impresa->N_CIVICO);
				$localizzazione->setCap($rs_impresa->CAP);
				$localizzazione->setProvincia($rs_impresa->SGL_PRV);
				if ($impTrovata->getProvincia() != null){
					if ($localizzazione->getProvincia() !=  $impTrovata->getProvincia())
						$localizzazione->setNREALoc($rs_impresa->REA_N_FUORI_PRV);
					else
						$localizzazione->setNREALoc($impTrovata->getNumeroREA());
					}
				//Prepara il tipo Sede
				if ($rs_impresa->C_T_UL_1 != null)
					$tipoSede = $rs_impresa->C_T_UL_1;
				if ($rs_impresa->C_T_UL_2 != null)
					$tipoSede .= ";" + $rs_impresa->C_T_UL_2;
				if ($rs_impresa->C_T_UL_3 != null)
					$tipoSede .= ";" + $rs_impresa->C_T_UL_3;
				if ($rs_impresa->C_T_UL_4 != null)
					$tipoSede .= ";" + $rs_impresa->C_T_UL_4;
				if ($rs_impresa->C_T_UL_5 != null)
					$tipoSede .= ";" + $rs_impresa->C_T_UL_5;
				$localizzazione->setTipoLocalizzazione($tipoSede);
				$localizzazione->setTelefono($rs_impresa->N_TELEFONO);
				$localizzazione->setFax($rs_impresa->N_TELEFAX);
				$localizzazione->setPec($rs_impresa->INDIRIZZO_PEC);

				if ($attivita != null && $attivita.length()>0){
					$localizzazione->setAttività($rs_impresa->desatt);
					$localizzazione->setTipologie($this->decodeTipiLocalizzazione($rs_impresa->C_T_UL_1, $rs_impresa->C_T_UL_2, $rs_impresa->C_T_UL_3,
						$rs_impresa->C_T_UL_4, $rs_impresa->C_T_UL_5));
				}
				$ls_local->push($localizzazione);
				/*dd($ls_local);
				exit;*/
			}
		}
		if ($imprese->count()==0 && !is_null($impTrovata)){
			if ($ls_local->count()>0) $impTrovata->setLocalizzazioni($ls_local);
			$imprese->push($impTrovata);
		}

		/*dd($imprese);
		exit;*/

		//Scrive l'XML
		$xml = $this->WriteXML($imprese);
		$formatter = Formatter::make($xml, Formatter::XML);
		Log::info('Interrogazione: '.implode(",", $request->all()));
		if ($json == null)
			return Response::make($xml, 200)->header('Content-Type', 'application/xml');
		else
			return Response::make($formatter->toJson(), 200)->header('Content-Type', 'application/json');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
