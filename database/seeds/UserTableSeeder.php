<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		// Insert some stuff
		DB::table('users')->insert(
			array(
				array(
					'name' => 'metrico',
					'email' => 'metrico@mi.camcom.it',
					'password' => Hash::make('metrico12!'),
					'verified' => true
				),
				array(
					'name' => 'digicamere',
					'email' => 'digicamere@digicamere.it',
					'password' => Hash::make('digimon12!'),
					'verified' => true
				)
			)
		);

	}

}
